// ==UserScript==
// @name        password_mangler
// @namespace   http://localhost
// @description password randomization tool
// @include     http://eng.utah.edu/~zarook/test.html
// @version     1
// @grant       none
// ==/UserScript==

// Adds a listener to password fields
// TODO: when a user changes the password, the randomizer has to be invoked
function setOnChange() {

    var inputFields = document.getElementsByTagName('input');
    var inputField = null;
    for (var i = 0; i < inputFields.length; ++i) {
        inputField = inputFields[i];
        if (inputField.type.toLowerCase() == 'password') {
            inputField.onchange = function () {
                alert("something changed!");
            };
        }
    }
}

setOnChange();