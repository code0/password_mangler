var data = require("sdk/self").data;
var store = require("sdk/simple-storage");


var settingsPanel = require("sdk/panel").Panel({
    contentURL: require("sdk/self").data.url("settings.html"),
    contentScriptFile: data.url("settings_script.js"),
    width: 250,
    height: 190
});

var wid = require("sdk/widget").Widget({
    id: "open-clock-btn",
    label: "Settings",
    contentURL: "http://www.mozilla.org/favicon.ico",
    panel: settingsPanel

});


        // Check the previous switch (on/off) value and adjust the panel only
        if (store.storage.on != null && store.storage.on == false) {
            settingsPanel.port.emit("switchOff");
            //worker.port.emit("switchedToContent", false);
        } else {
            settingsPanel.port.emit("switchOn");
            //worker.port.emit("switchedToContent", true);
        }

var helppanel = require("sdk/panel").Panel({
	width: 640,
	height: 480, 
 contentURL: require("sdk/self").data.url("index.html")

});
 
settingsPanel.port.on("help", function () {
            helppanel.show();
        });

var pageMod = require("page-mod").PageMod({
    include: ["*"],
    contentScriptFile: data.url("get_pwd.js"),
	attachTo: ["top", "frame","existing"],
    onAttach: function (worker) {

        // Check the previous switch (on/off) value and adjust the panel and mangler attachment
        if (store.storage.on != null && store.storage.on == false) {
            settingsPanel.port.emit("switchOff");
            worker.port.emit("switchedToContent", false);
        } else {
            settingsPanel.port.emit("switchOn");
            worker.port.emit("switchedToContent", true);
        }

        settingsPanel.port.on("switched", function (on) {
            if (on == true) {
                console.log("Randomizer was turned on.");
                store.storage.on = true;
            } else {
                console.log("Randomizer was turned off.");
                store.storage.on = false;
            }
            worker.port.emit("switchedToContent", on);
        });
    }
});
