// Adds listener to the on/off checkbox and triggers a signal to the addon
window.addEventListener('click', function (event) {
    var t = event.target;
    if (t.name == 'switch') {
        self.port.emit('switched', t.checked);
    }
    else if (t.name == 'help') {
        self.port.emit('help');
    }
}, false);


// Listen for signal from addon
self.port.on("switchOff", function () {
    console.log("switched off in panel");
    checkField = document.getElementById('onoffswitch');
    checkField.checked = false;
});

self.port.on("switchOn", function () {
    console.log("switched on in panel");
    checkField = document.getElementById('onoffswitch');
    checkField.checked = true;
});
